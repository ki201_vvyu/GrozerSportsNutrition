import {Field, Int, ObjectType} from '@nestjs/graphql';
import {Column, CreateDateColumn, Entity, ManyToMany, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';
import {categoriesConstants} from './categories.constants';
import {Product} from '../products/product.entity';

@Entity(categoriesConstants.tableName)
@ObjectType()
export class Category {
    @PrimaryGeneratedColumn()
    @Field(() => Int)
    id: number;

    @Column('boolean', {default: false})
    @Field(() => Boolean)
    isShown: boolean;

    @Column()
    @Field()
    name: string;

    @Column({unique: true})
    @Field()
    slug: string;

    @Column('text')
    @Field()
    description: string;

    @ManyToMany(() => Product, product => product.categories)
    @Field(() => [Product], {nullable: true})
    products: Product[];

    @CreateDateColumn({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)'})
    createdAt: Date;

    @UpdateDateColumn({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)', onUpdate: 'CURRENT_TIMESTAMP(6)'})
    updatedAt: Date;
}
